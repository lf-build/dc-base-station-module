var schema = {
    loanNumber: 'string:min=2,max=100,required',
};

function* get({
    $api,
    value: { loanNumber },
    $debug,
    $configuration,
    $stages: {
        'api': {
            'loan-accounting':{
                'get-payments': getPayments
            },
            'loan-management':{
                'get-schedule-info': getScheduleInfo
            }
        }
    }
}) {
    try {
        const versionResponse = yield getScheduleInfo.$execute
            .bind(undefined, {
                loanNumber,
            })({});
        const paymentInfo =  yield getPayments.$execute
        .bind(undefined, {
            loanNumber,
            scheduleVersion : versionResponse.scheduleVersionNo
        })({});
        return paymentInfo;

    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];