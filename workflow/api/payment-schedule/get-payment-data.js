var schema = {
    loanNumber: 'string:min=2,max=100,required',
};

function* get({
    $api,
    value: { loanNumber },
    $debug,
    $configuration,
    $stages: {
        'api': {
            'loan':{
                details
            },
            'loan-management':{
                'get-all-bank-info': bankData,
            }
        }
    }
}) {
    try {
        let finalResponse = {};

        const loanInfo = yield details.$execute
            .bind(undefined, {
                loanNumber,
            })({});

        const banks = yield bankData.$execute
            .bind(undefined, {
                loanNumber,
            })({});

        return Object.assign(loanInfo, {banks});
    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];