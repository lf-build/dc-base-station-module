var schema={
    applicationNumber:'string:required'
};

/**
 * 
 * Get profile details
 * 
 * @category Profile
 * @section API
 * @name get Borrower profile details
 * 
 * @api public
 * 
 */

function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
        'api': {
        'application':{
            'application-filter': applicationFilter
        }
        }
    }
}){
 try{
    
    const filterResponse = yield applicationFilter.$execute.bind(undefined, { applicationNumber })({});
    return filterResponse && filterResponse && filterResponse.owners && filterResponse.owners.length > 0 ? filterResponse.owners[0] : {};

  } catch(e) {
      $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,get];