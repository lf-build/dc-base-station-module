var schema={
  applicationNumber:'string:min=2,max=100,required',
};

/**
* 
* Get activity title list
* 
* @category Activity
* @section API
* @name get activity title list
* 
* @api public
* 
*/

function* get({
  $api,
  value:{applicationNumber},
  $debug,
  $configuration,
  $stages: {
    'api': {
        'application': {
          'filter': applicationFilter,
        }
    }
},
}){
  try{
        const { 'status-management': statusManagement } = yield $configuration('endpoints');

        const filterResponse = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});

        if(filterResponse.body && filterResponse.body.length > 0 && filterResponse.body[0].StatusWorkFlowId && filterResponse.body[0].StatusWorkFlowId != '' && filterResponse.body[0].StatusWorkFlowId != undefined && filterResponse.body[0].StatusWorkFlowId != null){
          const response = yield $api.get(`${statusManagement}/application/${filterResponse.body[0].StatusWorkFlowId}/${filterResponse.body[0].StatusCode}/activities`);
          return response.body;
        } else{
          const response = yield $api.get(`${statusManagement}/application/${applicationNumber}`);
          return response.body.activities;
        }
        
} catch(e) {
  $debug(e);
  throw {
    message: 'Something went wrong',
    code: 400,
  }
}
};

module.exports=[schema,get];