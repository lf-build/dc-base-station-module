const schema={
    applicationNumber:'string:min=2,max=100,required',
    isPrePayAddendums:'boolean',
    isNoStackingAddendums:'boolean'
};

function* get({
  value:{applicationNumber, isPrePayAddendums, isNoStackingAddendums},
  $api,
  $debug,
  $configuration,
}){
  const {'application-processor': applicationProcessor } = yield $configuration('endpoints');
  const payLoad = {isPrePayAddendums, isNoStackingAddendums};
  try{
    const response = yield $api.post(`${applicationProcessor}/application/${applicationNumber}/generate/agreement`, payLoad);
    return response;
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports=[schema,get];