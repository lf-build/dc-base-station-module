const schema={
    applicationNumber:'string:min=2,max=100,required'
};

function* get({
  value:{applicationNumber},
  $api,
  $debug,
  $configuration,
}){
  try{
    const {'application-processor': applicationProcessor } = yield $configuration('endpoints');
    const response = yield $api.get(`${applicationProcessor}/application/${applicationNumber}/send/agreement`);
    return response;
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports=[schema,get];