var FormData = require('form-data');
const schema = {
   applicationNumber: 'string:min=2,max=100,required',
   uploadFileType: 'string:min=2,max=100,required',
   fileList: {
        '@items': {
          file: 'string:min=2,max=100,required',
          dataUrl: 'string:min=2,max=100000000,required',
        },
        max: 20,
        min: 1,
    }
};

/**
 * 
 * Upload document
 * 
 * @category Application Document
 * @section API
 * @name upload
 * 
 * @api public
 * 
 */

function *set({
  $api, 
  value:{
    applicationNumber,
    fileList,
    uploadFileType,
  },
  $debug,
  $configuration,
  $stages: {
    'api': {
      'application': {
        'get-workflow': getWorkflow,
      }
    }
  }})  {

  let documentType = "";
  switch (uploadFileType)
  {
      case 'voidedcheck':
        documentType = "BankVerification/BankVerificationManual";
        break;
      case 'photoid':
        documentType = "IDVerification/IDVerificationManual";
        break;
      case 'signeddocument':
        documentType = "SignedDocumentVerification/SignedDocumentVerificationManual";
        break;
      case 'taxreturnstatement':
        documentType = "TaxReturnVerification/TaxReturnVerificationManual";
        break;
      case 'confessionofjudgment':
        documentType = "ConfessionVerification/ConfessionVerificationManual";
        break;
      case 'liendocument':
        documentType = "LienVerification/LienVerificationManual";
        break;
      case 'proofofpropertyownership':
        documentType = "ProofOfPropertyOwnerShipVerification/ProofOfPropertyOwnerShipVerification";
        break;
      case 'merchantstatement':
        documentType = "MerchantStatementVerification/MerchantStatementVerification";
        break; 
      default:
        break;
  }
  const {'verification-engine': documentUploadEngine, 'document-service': documentService } = yield $configuration('endpoints');

  try{
    const response = yield getWorkflow.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});
    const flowName =  (response && response !== '' ? `${response}/` : '');
    let finalResponse = [];
    for(index in fileList) {
        let fileObject = fileList[index];
        const formData = new FormData();
        formData.append('file', new Buffer(fileObject.dataUrl.substring(fileObject.dataUrl.indexOf("base64,")).replace('base64,',''), 'base64'), fileObject.file);
        if(documentType === ""){
          const response = yield $api.post(`${documentService}/${applicationNumber}/${uploadFileType}`, formData, null);
          finalResponse.push({
            category:response.body.category,
            fileId:response.body.documentId,
          });
        }
        else{
          const response = yield $api.post(`${documentUploadEngine}/application/${applicationNumber}/${flowName}${documentType}/upload-document`, formData, null); 
          finalResponse.push({
            category:response.body.category,
            fileId:response.body.documentId,
          });
        }
    }
    return finalResponse;
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports = [schema, set];