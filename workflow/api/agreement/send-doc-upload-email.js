var uuidV4 = require('uuid/v4');
var schema={
    applicationNumber:'string:min=2,max=100,required',
    documentList:{
        '@items': {
          category: 'string:min=2,max=100,required',
          fileId: 'string:min=2,max=100,required',
        },
        min: 1,
    },
};

function* get({
    $api,
    value:{applicationNumber,documentList},
    $debug,
    $configuration
}){
    try{
        const { eventhub,'doc-download-service': downloadEngine } = yield $configuration('endpoints');
        for(index in documentList) {
            const key = uuidV4();
            let fileObject = documentList[index];
            let fileName = "";
            try{
                const documentDetails = yield $api.get(`${downloadEngine}/application/${applicationNumber}/${fileObject.fileId}`);
                fileName=documentDetails.body.fileName;
            }catch(ex){
                $debug(ex);
            }
            const payLoad = {
                "EntityId" : applicationNumber,
                "EntityType" : "application",
                "DocumentCategory" : fileObject.category,
                "ReferenceNumber" : key,
                "UploadDocFileId" :fileObject.fileId,
                "UploadDocFileName" : fileName,
            }
            const response = yield $api.post(`${eventhub}/BorrowerDocumentUploaded`,payLoad);
        }
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];