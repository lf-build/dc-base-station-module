var schema={
    applicationNumber:'string:min=2,max=100,required',
    returnUrl:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{applicationNumber,returnUrl},
    $debug,
    $configuration
}){
    try{
          const { 'application-processor' : applicationProcessor } = yield $configuration('endpoints');
          const response = yield $api.post(`${applicationProcessor}/application/${applicationNumber}/get/agreement`,{returnUrl});
          return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];