const schema = {
    applicationNumber: 'string:min=2,max=100,required',
};


/**
 * 
 * Resend docuSign mail
 * 
 * @category DocuSign
 * @section API
 * @name resend docuSign mail
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value:{applicationNumber}, 
    $debug,  
    $configuration
}){
 try{
  const { 'application-processor': applicationProcessor } = yield $configuration('endpoints');
  const response = yield $api.get(`${applicationProcessor}/application/${applicationNumber}/generate/agreement`);
  return response.body ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,set];