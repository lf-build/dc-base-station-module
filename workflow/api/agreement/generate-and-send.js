const schema={
    applicationNumber:'string:min=2,max=100,required',
    isPrePayAddendums:'boolean',
    isNoStackingAddendums:'boolean'
};

function* get({
  $debug,
  value:{applicationNumber,isPrePayAddendums,isNoStackingAddendums},
  $stages: {
    'api': {
      'agreement': {
        'generate': generateAgreement,
        'send': sendAgreement,
      }
    }
  }
}){
  try{
    const generateResponse = yield generateAgreement.$execute
    .bind(undefined, {
      applicationNumber,
      isPrePayAddendums,
      isNoStackingAddendums
    })({});
    const sendResponse = yield sendAgreement.$execute
    .bind(undefined, {
        applicationNumber
    })({});
    return {generateResponse, sendResponse};
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports=[schema,get];