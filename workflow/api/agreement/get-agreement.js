const schema={
      applicationNumber:'string:min=2,max=100,required'
};

function* get({
    value:{applicationNumber},
    $api,
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter,
        'required-document': stipulationDoc,
        'get-workflow': getWorkflow,
      },
      'document':{
        'get-fact-documents': getFactDocuments,
      }
    }
  },
}){
    const {'verification-engine': verificationEngine,'document-service': documentService } = yield $configuration('endpoints');

    //Fetch list of addition stipulation to show
    let taxReturnVerification = false;
    let confessionVerification = false;
    let lienVerification = false;
    let contractDocsVerification= false;
    let merchantStatementVerification = false;
    let proofOfPropertyOwnerShipVerification = false;
    let photoIdFactVerificationStatus= false;
    let voidedCheckFactVerificationStatus=false;
    let contractFactVerificationStatus= false;
    let taxReturnVerificationStatus= false;
    let lienVerificationStatus= false;
    let confessionVerificationStatus= false;
    let proofOfPropertyVerificationStatus= false;
    let merchantstatementVerificationStatus=false;

    try{
        const stipulationList = yield stipulationDoc.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(stipulationList.length > 0){
            stipulationList.forEach(function(element) {
                if(element.factName === 'TaxReturnVerification'){
                   taxReturnVerification = element.isRequested;
                }
                else if(element.factName === 'ConfessionVerification'){
                   confessionVerification = element.isRequested;
                }
                else if(element.factName === 'LienVerification'){
                   lienVerification = element.isRequested;
                   
                }
                else if(element.factName === 'ProofOfPropertyOwnerShipVerification'){
                    proofOfPropertyOwnerShipVerification = element.isRequested;
                    
                 }
                 else if(element.factName === 'MerchantStatementVerification'){
                    merchantStatementVerification = element.isRequested;
                    
                 }
            }, this);
        } 
    }catch(e){
        $debug(e);
    }
    
    //fetch Voided check documents
    let voidedCheckDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'BankVerification' })({});
        voidedCheckDocs = factRespsone.files;
        voidedCheckFactVerificationStatus = factRespsone.verificationStatus; 
    }catch(e){
        $debug(e);
    }

    //fetch PhotoId documents
    let photoIdDocs = []; 
    try{ 
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'IDVerification' })({});
        photoIdDocs = factRespsone.files;
        photoIdFactVerificationStatus = factRespsone.verificationStatus; 
    }catch(e){
        $debug(e);
    }

    //fetch Contract documents
    let contractDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'SignedDocumentVerification' })({});
        contractDocs = factRespsone.files;
        contractFactVerificationStatus = factRespsone.verificationStatus; 
    }catch(e){
        $debug(e);
    }

    //fetch Tax return documents
    let taxReturnVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'TaxReturnVerification' })({});
        taxReturnVerificationDocs = factRespsone.files;
        taxReturnVerificationStatus = factRespsone.verificationStatus;
        if(factRespsone.files.length > 0){
            taxReturnVerification = true;
        }
    }catch(e){
       $debug(e);
    }

    //fetch lien documents
    let lienVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'LienVerification' })({});
        lienVerificationDocs = factRespsone.files;
        lienVerificationStatus = factRespsone.verificationStatus;
        if(factRespsone.files.length > 0){
            lienVerification = true;
        }
    }catch(e){
        $debug(e);
    }

     //fetch confession documents
    let confessionVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'ConfessionVerification' })({});
        confessionVerificationDocs = factRespsone.files;
        confessionVerificationStatus = factRespsone.verificationStatus;
        if(factRespsone.files.length > 0){
            confessionVerification = true;
        }
    }catch(e){
        $debug(e);
    }

     // fetch proofofproperty documents
     let proofOfPropertyOwnerShipVerificationDocs = []; 
     try{
         const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'ProofOfPropertyOwnerShipVerification' })({});
         proofOfPropertyOwnerShipVerificationDocs = factRespsone.files;
         proofOfPropertyVerificationStatus = factRespsone.verificationStatus;
        if(factRespsone.files.length > 0){
            proofOfPropertyOwnerShipVerification = true;
        }
     }catch(e){
         $debug(e);
     }

    //fetch merchantstatement documents
     let merchantstatementVerificationDocs = []; 
     try{
         const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'MerchantStatementVerification' })({});
         merchantstatementVerificationDocs = factRespsone.files;
         merchantstatementVerificationStatus = factRespsone.verificationStatus;
        if(factRespsone.files.length > 0){
            merchantStatementVerification = true;
        }
     }catch(e){
         $debug(e);
     }


     let miscellaneousVerificationDocs = []; 
     try{
        
        const miscellaneousVerificationFiles = yield $api.get(`${documentService}/${applicationNumber}/other`);
         for(index in miscellaneousVerificationFiles.body){
            const miscellaneousVerificationDoc = {
                fileName: miscellaneousVerificationFiles.body[index].fileName,
                fileId: miscellaneousVerificationFiles.body[index].id,
                }
            miscellaneousVerificationDocs.push(miscellaneousVerificationDoc);
            }
     }catch(e){
         $debug(e);
     }

    let email = '';
    let docuSignStatus = 'Signed';
    let statusCode = '';
    let finalizationStatus = true;
    try{
       const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(response.body && response.body.length > 0){
            statusCode = response.body[0].StatusCode;
            if(statusCode ==='300.10'){
                contractDocsVerification= true;
            }
            email = (response.body[0].Owners && response.body[0].Owners.length > 0) ? response.body[0].Owners[0].EmailAddress : '';
            if(response.body[0].tags && response.body[0].tags.length > 0){
                if(response.body[0].tags.filter((p) => p === 'Pending Signature').length > 0){
                    docuSignStatus = 'Unsigned';
                }
                if(response.body[0].tags.filter((p) => p === 'Pending Signature').length > 0 || response.body[0].tags.filter((p) => p === 'Pending Documents').length > 0){
                    finalizationStatus = false;
                }
            }
        }
    }
    catch(e){
         $debug(e);
    }
    return {
        "voidedCheck":{
            "status":voidedCheckDocs.length > 0 ? true : false,
            "files":voidedCheckDocs,
            "verificationStatus":voidedCheckFactVerificationStatus,  
        },
        "photoId":{
            "status":photoIdDocs.length > 0 ? true : false,
            "files":photoIdDocs,
            "verificationStatus":photoIdFactVerificationStatus,
        },
        "contract":{
            "status": (contractDocs.length > 0 || docuSignStatus === 'Signed' ) ? true : false,
            "files":contractDocs,
            docuSignStatus,
            "email": email,
            "isDisplay":contractDocsVerification,
            "verificationStatus":contractFactVerificationStatus,
            
        },
        "taxReturn":{
            "status":taxReturnVerificationDocs.length > 0 ? true : false,
            "files":taxReturnVerificationDocs,
            "isDisplay":taxReturnVerification,
            "verificationStatus":taxReturnVerificationStatus,  
        },
        "lien":{
            "status":lienVerificationDocs.length > 0 ? true : false,
            "files":lienVerificationDocs,
            "isDisplay":lienVerification,
            "verificationStatus":lienVerificationStatus,
        },
        "confession":{
            "status":confessionVerificationDocs.length > 0 ? true : false,
            "files":confessionVerificationDocs,
            "isDisplay":confessionVerification,
            "verificationStatus":confessionVerificationStatus,
        },
        "proofofproperty":{
            "status":proofOfPropertyOwnerShipVerificationDocs.length > 0 ? true : false,
            "files":proofOfPropertyOwnerShipVerificationDocs,
            "isDisplay":proofOfPropertyOwnerShipVerification,
            "verificationStatus":proofOfPropertyVerificationStatus,
        },
        "merchant":{
            "status":merchantstatementVerificationDocs.length > 0 ? true : false,
            "files":merchantstatementVerificationDocs,
            "isDisplay":merchantStatementVerification,
            "verificationStatus":merchantstatementVerificationStatus,
        },
        "miscellaneous":{
            "status":miscellaneousVerificationDocs.length > 0 ? true : false,
            "files":miscellaneousVerificationDocs,
        },
        finalizationStatus,
        statusCode
    };
};

module.exports=[schema,get];