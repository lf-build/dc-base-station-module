var schema={
    applicationNumber:'string:min=2,max=100,required',
    tags:{
        '@items': 'string:min=2,max=100,required',
     }
};

function* set({
    $api,
    value:{applicationNumber,tags},
    $debug,
    $configuration
}){
    try{
          const { 'application-filter' : applicationFilter } = yield $configuration('endpoints');
          let tagsArrayToSting = '';
          for(index in tags) {
            tagsArrayToSting= `${tagsArrayToSting}/${tags[index]}`;
          }
          const response = yield $api.post(`${applicationFilter}/removetags/${applicationNumber}${tagsArrayToSting}`);
          return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,set];