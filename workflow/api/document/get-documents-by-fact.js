var schema={
    applicationNumber:'string:min=2,max=100,required',
    factName:'string:min=2,max=100,required',
};
    
function* get({
    $api,
    value:{applicationNumber,factName},
    $debug,
    $configuration,
    $stages: {
        'api': {
            'application': {
                'get-workflow': getWorkflow,
            }
        }
    }
}){
    try{
          const {'verification-engine': verificationEngine } = yield $configuration('endpoints');

          let finalResponse = {
            files: []
          };

          const response = yield getWorkflow.$execute
            .bind(undefined, {
                applicationNumber: applicationNumber,
            })({});
          const flowName =  (response && response !== '' ? `${response}/` : '');

          try{
          
                const factRespsone = yield $api.get(`${verificationEngine}/application/${applicationNumber}/${flowName}${factName}/documentsDetails`);
                return factRespsone.body;
            }catch(ex){
                $debug(ex);
            }

            
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
};
    
module.exports=[schema,get];