const schema = {
   applicationNumber: 'string:min=2,max=100,required',
   category: 'string:min=2,max=100,required',
};

/**
 * 
 * Get document
 * 
 * @category Application Document
 * @section API
 * @name get document by category
 * 
 * @api public
 * 
 */

function *set({
  $api, 
  value:{
    applicationNumber,
    category,
  },
  $debug,
  $configuration,
  $stages: {
    'api': {
      'application': {
        'required-document': stipulationDoc,
        'get-workflow': getWorkflow,
      },
      'document':{
          'get-documents-by-fact': getDocumentsByFact
      },
      'application-document':{
          'get-category-docs': getDocumentsByCategory
      },
      'agreement':{
          'download-file':getFile
      }
    }
  },
}){
    
  try{

    const stipulationList = yield stipulationDoc.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});

    const response = yield getWorkflow.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});
    const flowName =  (response && response !== '' ? `${response}/` : '');

    const factObject = stipulationList.filter(function (obj) {
        return obj.factName === category;
    });
    
    if(factObject.length > 0) {
        
        const finalResponse = [];
      const factResponse = yield getDocumentsByFact.$execute
      .bind(undefined, {
          applicationNumber: applicationNumber,
          factName: category,
      })({});
      if(factResponse) {
    
      for(fact in factResponse){
        const fileResponse = yield getFile.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
            fileId: factResponse[fact].id,
        })({});
        const currentFact = {
            id: factResponse[fact].id,
            fileName: factResponse[fact].fileName,
            size: factResponse[fact].size,
            tags: factResponse[fact].tags,
            timestamp: factResponse[fact].timestamp,
            metadata: factResponse[fact].metadata,
            downloadString: fileResponse.downloadString,
        }
        finalResponse.push(currentFact);
      }
    }
      return finalResponse;

    } else {
        const categoryResponse = yield getDocumentsByCategory.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
            category: category,
        })({});
        return categoryResponse;
    }
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports = [schema, set];