const schema={
    applicationNumber:'string:min=2,max=100,required'
};

function* get({
  value:{applicationNumber},
  $api,
  $debug,
  $configuration,
  $stages: {
        'api': {
            'application': {
                'required-document': stipulationDoc,
                'get-workflow': getWorkflow,
            }
        }
    },
}){
  const {'verification-engine': verificationEngine } = yield $configuration('endpoints');

  let finalResponse = [];

  try{
    const response = yield getWorkflow.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});
    const flowName =  (response && response !== '' ? `${response}/` : '');
    
    const stipulationList = yield stipulationDoc.$execute
      .bind(undefined, {
          applicationNumber: applicationNumber,
      })({});
      if(stipulationList.length > 0){
        for(indx in stipulationList) {
            const element = stipulationList[indx];
            let factInfo = {};
            factInfo.factName = element.factName;
            factInfo.methodName = element.methodName;
            factInfo.documentName = element.documentName[0];
 
            let finalDocs = []; 
            try {
                const files = yield $api.get(`${verificationEngine}/application/${applicationNumber}/${flowName}${element.factName}/documentsDetails`);
                for(index in files.body){
                    if(files.body[index].documentStatus === 'Verifiable'){
                        const finalDoc = {
                            fileName: files.body[index].fileName,
                            fileId: files.body[index].id,
                        }
                        finalDocs.push(finalDoc);
                    }
                }
            } catch(error){
                $debug(error);
            }

            factInfo.files = finalDocs;
            finalResponse.push(factInfo);
          }
      } 
  }catch(e){
      $debug(e);
  }
  return finalResponse;
};

module.exports=[schema,get];