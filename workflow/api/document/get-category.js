const schema={
    applicationNumber:'string:min=2,max=100,required'
};

/**
 * 
 * Get category list
 * 
 * @category Application Document
 * @section API
 * @name get document category
 * 
 * @api public
 * 
 */

function* get({
    $api,
    $debug,
    value:{applicationNumber},
    $configuration,
    $stages: {
        'api': {
          'application': {
            'required-document': stipulationDoc,
          }
        }
      },
}){
    try{
        let finalResponse = [];

        const stipulationList = yield stipulationDoc.$execute
          .bind(undefined, {
              applicationNumber: applicationNumber,
          })({});

        if(stipulationList.length > 0){
            for(indx in stipulationList) {
                const element = stipulationList[indx];
                finalResponse.push(element.factName);
            }
        }

        return finalResponse;
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];