var schema={
    applicationNumber:'string:min=2,max=100,required',
};
    
function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
}){
    try{
          //const {'verification-engine': verificationEngine } = yield $configuration('endpoints');

          let finalResponse = {
            documents : [],
          };

          try{
            // Endpoint call to fetch documents
            // const factRespsone = yield $api.get(`${verificationEngine}/application/${applicationNumber}/${flowName}${factName}/documentsDetails`);
          }catch(ex){
            $debug(ex);
          }

          const { docs } = yield $configuration('transform-temp-documents');
          finalResponse.documents = docs;

          const { transform: {documents} } = yield $configuration('borrower');
          finalResponse.transformHelper = documents;

          return finalResponse;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
};

module.exports=[schema,get];