var schema={
    applicationNumber:'string:min=2,max=100,required',
    factName:'string:min=2,max=100,required',
};
    
function* get({
    $api,
    value:{applicationNumber,factName},
    $debug,
    $configuration,
    $stages: {
        'api': {
            'application': {
                'get-workflow': getWorkflow,
            }
        }
    }
}){
    try{
          const {'verification-engine': verificationEngine } = yield $configuration('endpoints');

          let finalResponse = {
            files: [],
            verificationStatus: false
          };

          const response = yield getWorkflow.$execute
            .bind(undefined, {
                applicationNumber: applicationNumber,
            })({});
          const flowName =  (response && response !== '' ? `${response}/` : '');

          try{
          
                const factRespsone = yield $api.get(`${verificationEngine}/application/${applicationNumber}/${flowName}${factName}/documentsDetails`);
                for(index in factRespsone.body){
                    if(factRespsone.body[index].documentStatus === 'Verifiable'){
                            const file = {
                                fileName: factRespsone.body[index].fileName,
                                fileId: factRespsone.body[index].id,
                            }
                            finalResponse.files.push(file);
                        }
                    }
                
                const verificationStatusResposne = yield $api.get(`${verificationEngine}/application/${applicationNumber}/${flowName}${factName}/status`);
                if(verificationStatusResposne.body.verificationStatus === 'Passed'){
                    finalResponse.verificationStatus = true;
                }
            }catch(ex){
                $debug(ex);
            }

            return finalResponse;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
};
    
module.exports=[schema,get];