var FormData = require('form-data');
const schema = {
   applicationNumber: 'string:min=2,max=100,required',
   uploadFileType: 'string:min=2,max=100,required',
   fileList: {
        '@items': {
          file: 'string:min=2,max=100,required',
          dataUrl: 'string:min=2,max=100000000,required',
        },
        max: 20,
        min: 1,
    }
};

/**
 * 
 * Upload document
 * 
 * @category Application Document
 * @section API
 * @name upload
 * 
 * @api public
 * 
 */

function *set({
  $api, 
  value:{
    applicationNumber,
    fileList,
    uploadFileType,
  },
  $debug,
  $configuration,
  $stages: {
    'api': {
      'application': {
        'required-document': stipulationDoc,
        'get-workflow': getWorkflow,
      }
    }
  },
}){

  const {'verification-engine': documentUploadEngine, 'document-service': documentService } = yield $configuration('endpoints');

  try{

    const stipulationList = yield stipulationDoc.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});

    const response = yield getWorkflow.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});
    const flowName =  (response && response !== '' ? `${response}/` : '');

    const factObject = stipulationList.filter(function (obj) {
        return obj.factName === uploadFileType;
    });
    
    let callingUrl = '';
    if(factObject.length > 0)    {
      const encodedFactName = encodeURI(factObject[0].factName);
      const encodedMethodName = encodeURI(factObject[0].methodName);
        callingUrl = `${documentUploadEngine}/application/${applicationNumber}/${flowName}${encodedFactName}/${encodedMethodName}/upload-document`;
    } else{
      const encodedUploadFileType = encodeURI(uploadFileType);
        callingUrl = `${documentService}/${applicationNumber}/${encodedUploadFileType}`;
    }

    for(index in fileList) {
            let fileObject = fileList[index];
            const formData = new FormData();
            formData.append('file', new Buffer(fileObject.dataUrl.substring(fileObject.dataUrl.indexOf("base64,")).replace('base64,',''), 'base64'), fileObject.file);
            const response = yield $api.post(callingUrl, formData, null); 
    }   
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports = [schema, set];