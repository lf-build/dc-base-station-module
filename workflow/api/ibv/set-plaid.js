const schema={
    "providerName": "string",
    "providerToken":  "string",
    "type": "string",
    "firstName": "string",
    "lastName": "string",
    "userMailId": "string",
    "linkSessionId": "string",
    "applicationNumber":"string",
    "accountId":"string",
    "bankId":"string",
};

/**
 * 
 * Set Plaid Bank
 * 
 * @category IBV Bank Linking
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value,
    $debug,
    $configuration
}){

   try{
         const { 'ibv-banklinking': ibv } = yield $configuration('endpoints');
         const applicationNumber = value.applicationNumber;
         delete value.applicationNumber;
         const response = yield $api.post(`${ibv}/application/${applicationNumber}/connect`, value);
         return response.body ;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];