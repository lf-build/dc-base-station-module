const schema={
    finicityAppToken : "string",
    customerId : "string",
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get finicity transections
 * 
 * @category IBV Bank Linking
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value:{
        finicityAppToken,
        customerId,
        applicationNumber,
      },
    $debug,
    $configuration
}){
   try{
        const {  'ibv-banklinking' : ibv } = yield $configuration('endpoints');
        const payLoad = { finicityAppToken, customerId };
        const transactionPull = yield $api.post(`${ibv}/application/${applicationNumber}/finicity/process`, payLoad);
        return transactionPull.body;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];