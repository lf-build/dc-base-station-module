var schema={
    applicationNumber:'string:min=2,max=100,required',
    };

/**
 * 
 * Get Plaid Public Token
 * 
 * @category IBV Bank Linking
 * @section API
 * @name get
 * 
 * @api public
 * 
 */

    function* get({
           $api,
           value:{applicationNumber},
           $debug,
           $configuration
    }){
    try{
          const {  'ibv-banklinking' : ibv } = yield $configuration('endpoints');
          const response = yield $api.get(`${ibv}/${applicationNumber}/plaidRefresh`);
          return response.body;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
    };
    
    module.exports=[schema,get];