const schema = {
    config:'string:required',
};

/**
 * 
 * Add activity
 * 
 * @category Activity
 * @section API
 * @name add
 * 
 * @api public
 * 
 */


function* set({
    $api,
    value:{config}, 
    $debug,  
    $configuration
}){
 try{
  const { borrower } = yield $configuration(config);
  return borrower;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Configuration not found',
      code: 400,
    }
  }
}

module.exports=[schema,set];