var schema = {
    loanNumber: 'string:min=2,max=100,required',
};

function* get({
    $api,
    value: { loanNumber },
    $debug,
    $configuration,
    $stages: {
        'api': {
            'loan-accounting':{
                'get-economics-data': getEconomics,
                'get-fees': getFees,
            },
            'loan-management':{
                'get-schedule-info': getScheduleInfo,
                'get-loan-details': getLoanInfo
            },
            'payment-processor':{
                'get-loan-inprogress': getInprogressPayments
            }
        }
    }
}) {
    try {
        let finalResponse = {};

        const loanInfo = yield getLoanInfo.$execute
            .bind(undefined, {
                loanNumber,
            })({});

        finalResponse.loanInfo = loanInfo;
        
        const scheduleInfo = yield getScheduleInfo.$execute
            .bind(undefined, {
                loanNumber,
            })({});
        finalResponse.scheduleInfo = scheduleInfo;
        
        const economicsData =  yield getEconomics.$execute
        .bind(undefined, {
            loanNumber,
            scheduleVersion : scheduleInfo.scheduleVersionNo
        })({});
        finalResponse.economicsData = economicsData;

        const feesResponse = yield getFees.$execute
            .bind(undefined, {
                loanNumber,
            })({});
        finalResponse.fees = feesResponse;

        const inprogressPayments = yield getInprogressPayments.$execute
        .bind(undefined, {
            loanNumber,
        })({});
        finalResponse.inprogressPayments = inprogressPayments;
        return finalResponse;

    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];