const schema={
    name:'string:min=2,max=100,required',
};

function* get({
  value,
  $api,
  $debug,
  $configuration,
}){
  const {lookupservice: lookup} = yield $configuration('endpoints');
  try{
      const lookupResponse = yield $api.get(`${lookup}/${value.name}`);
      return lookupResponse.body;
  }catch(e){
      $debug(e);
      throw {
          message: 'Something went wrong',
          code: 400,
      }
  }
};

module.exports=[schema,get];