var schema={
    panNumber:'string:required',
    borrowerToken:'string:required',
};
  
  /**
   * 
   * Create lead
   * 
   * @category Lead
   * @section API
   * @name create lead
   * 
   * @api public
   * 
   */
  
  function* set({
      $api,
      value, 
      $debug,  
      $configuration
  }){
    try {
      
        const { 'applicant-detail': applicantDetail } = yield $configuration('endpoints');
        const appResponse = yield $api.get(`${applicantDetail}/applicant/${value.borrowerToken}/${value.panNumber}`);
        return appResponse.body;
  
    } catch(e) {
      $debug(e);
      if(e && e.body && e.body.code && e.body.code === 409){
        throw {
          message: 'Email Address already exist.',
          code: e.body.code,
        }
      }
      throw {
        message: e && e.body && e.body.message ? e.body.message : 'Something went wrong',
        code: 400,
      }
    }
  }
  
  module.exports=[schema,set];