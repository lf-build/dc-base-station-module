var schema={
    applicantId:'string:required',
    userId:'string:required',
};
  
  /**
   * 
   * Associate User
   * 
   * @category Lead
   * @section API
   * @name create lead
   * 
   * @api public
   * 
   */
  
  function* set({
      $api,
      value, 
      $debug,  
      $configuration
  }){
    try {
      
        const { 'applicant-detail': applicantDetail } = yield $configuration('endpoints');
        const appResponse = yield $api.put(`${applicantDetail}/${value.applicantId}/associate/user/${value.userId}`, {});
        return appResponse.body;
  
    } catch(e) {
      $debug(e);
      if(e && e.body && e.body.code && e.body.code === 409){
        throw {
          message: 'User already exist.',
          code: e.body.code,
        }
      }
      throw {
        message: e && e.body && e.body.message ? e.body.message : 'Something went wrong',
        code: 400,
      }
    }
  }
  
  module.exports=[schema,set];