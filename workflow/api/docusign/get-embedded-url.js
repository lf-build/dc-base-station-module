const schema = {
    applicationNumber: 'string:required',
    documentId:'string:required',
    receipients:{
        '@items':{name :'string:min=2,max=100,required',
                  email: 'email:required'}
             },
    emailBody:'string:required',
    SignerEmail: 'email:required',
    ClientUserId:'string:required',
    SignerName:'string:required',
    ReturnUrl:'string:required'
};


function* set({
    $api,
    value: {
      applicationNumber, documentId, receipients, emailBody, SignerEmail,ClientUserId,SignerName, 
      ReturnUrl
    },
    $debug,
    $configuration,
    $stages: {
        'api': {
          'docusign': {
            'embedded-view-generate': viewGenerate,
            'embedded-url-generate': urlGenerate
          }
        }
      },
}){
    try{
        const viewResponse = yield viewGenerate.$execute
        .bind(undefined, {
            applicationNumber,
            documentId,
            receipients,
            emailBody
        })({});
        const EnvelopeId = viewResponse.envelopeSummary.envelopeId;
        const response = yield urlGenerate.$execute
         .bind(undefined, {
            applicationNumber,
            SignerEmail,
            ClientUserId,
            EnvelopeId,
            SignerName,
            ReturnUrl
           })({});

        return viewResponse;

        } catch(e) {
          $debug(e);
          throw e;
        }
}

module.exports=[schema,set];