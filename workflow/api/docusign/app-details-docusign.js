const schema = {
    applicationNumber: 'string:required',
    returnUrl: 'string:required'
};


function* get({
    $api,
    value:{applicationNumber,returnUrl},
    $debug,  
    $configuration
}){
    try{
        const { 'application-processor' : applicationProcessor } = yield $configuration('endpoints');
        const response = yield $api.post(`${applicationProcessor}/application/appdetailsdocusign/initialize/application/${applicationNumber}`,{returnUrl});
        return response.body;
    } catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,get];