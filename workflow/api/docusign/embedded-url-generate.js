const schema = {
    applicationNumber: 'string:required',
    SignerEmail: 'email:required',
    ClientUserId:'string:required',
    EnvelopeId:'string:required',
    SignerName:'string',
    ReturnUrl:'string'
};


function* set({
    $api,
    value:{applicationNumber,SignerEmail,ClientUserId,EnvelopeId,SignerName,ReturnUrl},
    $debug,  
    $configuration
}){
    try{
        const { syndication } = yield $configuration('endpoints');
        const response = yield $api.post(`${syndication}/application/${applicationNumber}/docusign/embeddedurlgenerate`, {
            SignerEmail,
            ClientUserId,
            EnvelopeId,
            SignerName,
            ReturnUrl
        });
        return response.body;
    } catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,set];