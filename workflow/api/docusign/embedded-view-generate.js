const schema = {
    applicationNumber: 'string:required',
    documentId:'string:required',
    receipients:{
        '@items':{
            name :'string:min=2,max=100,required',
            email:'string:required'}
        },
	emailBody:'string',
};


function* set({
    $api,
    value:{applicationNumber,documentId,receipients,emailBody}, 
    $debug,  
    $configuration
}){
    try{
        const { syndication } = yield $configuration('endpoints');
        const response = yield $api.post(`${syndication}/application/${applicationNumber}/docusign/embeddedviewgenerate`, {
            documentId,
            receipients,
            emailBody
        });
        return response.body ;
        } 
    catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,set];