const schema={
    password:'string:min=2,max=100,required',
    oldPassword:'string:min=2,max=100,required',
};

function* set({
    $api,
    value:{ userName, password, oldPassword },
    $debug,
    $configuration,
  }
){
   try{
    const { identity } = yield $configuration('endpoints');
    const payload = {
        CurrentPassword: oldPassword,
        Password: password,
        ConfirmPassword: password,
    };
    const response=yield $api.put(`${identity}/change-password`,payload);
    return response.body;
   }catch(e){
       $debug(e);
       return{
           code : 500,
           message: e,
       }
   }
};

module.exports=[schema,set];