const schema={
    name:'string:required',
    email:'string:required',
    username:'string:required',
    password:'string:required',
    roles:'array',
};

function* set({
    $api,
    value,
    $debug,
    $configuration,
  }
){
   try{
    const { identity } = yield $configuration('endpoints');
    const payload = {
        name:value.name,
        email:value.email,
        username:value.username,
        roles:value.roles,
        password:value.password
    };
    const response=yield $api.put(`${identity}`,payload);
    return response.body;
   }catch(e){
       $debug(e);
       return{
           code : 500,
           message: e,
       }
   }
};

module.exports=[schema,set];