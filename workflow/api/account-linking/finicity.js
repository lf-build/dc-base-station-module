const schema={
    firstName : "string",
    lastName : "string",
    userName : "string",    
    redirectUri : "string",
    type : "string",
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Set Finicity Generate Token, Add Customer & Open Connect
 * 
 * @category Bank Linking
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value:{
        firstName,
        lastName,
        userName,
        redirectUri,
        type,
        applicationNumber,
      },
    $debug,
    $configuration
}){
    
   try{
         const { finicity } = yield $configuration('endpoints');
         const accessToken = yield $api.get(`${finicity}/application/${applicationNumber}/accesstoken`);
         if(accessToken.body && accessToken.body.accessToken) {             
            const finicityAppToken = accessToken.body.accessToken;
            const addCustomerPayLoad = {finicityAppToken, firstName, lastName, userName };
            const addCustomer = yield $api.post(`${finicity}/application/${applicationNumber}/customer/add`, addCustomerPayLoad);
            if(addCustomer) {
                const customerId = addCustomer.body.customerId;
                const connectPayLoad = {finicityAppToken, customerId, redirectUri, type};
                const connectResponse = yield $api.post(`${finicity}/application/${applicationNumber}/connect/generate`, connectPayLoad);
                    const response = {
                        link: connectResponse.body.link,
                        customerId: customerId,
                        accessToken: finicityAppToken,
                    }
                return response;
             }
        }
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];