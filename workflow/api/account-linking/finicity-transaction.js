const schema={
    finicityAppToken : "string",
    customerId : "string",
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get finicity transections
 * 
 * @category Bank Linking
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value:{
        finicityAppToken,
        customerId,
        applicationNumber,
      },
    $debug,
    $configuration
}){
   try{
        const { finicity } = yield $configuration('endpoints');
        const payLoad = { finicityAppToken, customerId };
        const transactionPull = yield $api.post(`${finicity}/application/${applicationNumber}/customer/transactions/fetch`, payLoad);
        return transactionPull.body;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];