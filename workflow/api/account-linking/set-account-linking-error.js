const schema={
    Institution : {
        type : "string",
        name : "string"
    },
    LinkRequestId : "string",
    LinkSessionId : "string",
    Status : "string",
    EntityType : "string",
    EntityId : "string",

};

/**
 * 
 * Set Account Linking Error
 * 
 * @category Bank Linking
 * @section API
 * @name Account linking error
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value,
    $debug,
    $configuration
}){

   try{
         const { 'webhook-notifier': webhook, configuration } = yield $configuration('endpoints');
         const tokenResponse =  yield $api.get(`${configuration}/webhook-notifier`);
         const response = yield $api.post(`${webhook}/clienttrack?token=${tokenResponse.body.WebhookTenantTokenId}`, value);
         return response.body ;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];