const schema={
    PublicToken : "string",
    BankSupportedProductType : "string",
    AccountId : "string",
    EntityId : "string",
    EntityType : "string",
    InstitutionId : "string",
    InstitutionName : "string",
};

/**
 * 
 * Set Plaid Bank
 * 
 * @category Bank Linking
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value,
    $debug,
    $configuration
}){

   try{
         const { 'webhook-notifier': webhook } = yield $configuration('endpoints');
         const tokenResponse =  yield $configuration('webhook-notifier');
         const response = yield $api.post(`${webhook}/plaid?token=${tokenResponse.body.WebhookTenantTokenId}`, value);
         return response.body ;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];