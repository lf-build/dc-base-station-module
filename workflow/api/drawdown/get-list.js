const schema = {
    referenceId: 'number',
    pageSize: 'number',
};

/**
 * 
 * Get drawdown list
 * 
 * @category Drawdown
 * @section API
 * @name get all
 * 
 * @api public
 * 
 */

function * get({
    $api,
    value: {
        referenceId, pageSize,
    },
    $debug,
    $configuration
}) {
    try{
        const { 'application-filters': applicationFilters  } = yield $configuration('endpoints');
        const filter = {
            "searchFilterView": {},
            "sorts": {
                'DrawDownDate.Time.Tick' : 1
            }
        };

        if (referenceId) {
            filter.searchFilterView = {
                        'DrawDownDate.Time.Ticks' : {
                            "$gt": referenceId,
                        }
                    }
        }
    
        const response = yield $api.post(`${applicationFilters}/drawdown/search-applications-free?page=${1}&pageSize=${pageSize || 100}`, filter);
        return {
            referenceId: response.body.length > 0 ? response.body[response.body.length - 1].DrawDownDate.Time.Ticks : referenceId,
            items: response.body,
            pageSize: response
                .headers
                .get('X-PAGE-SIZE'),
            totalCount: response
                .headers
                .get('X-TOTAL-COUNT'),
            totalPages: response
                .headers
                .get('X-TOTAL-PAGES')

        };
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];