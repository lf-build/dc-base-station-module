var schema={
    drawDownId:'string:min=2,max=100,required',
    };
    
    /**
    * 
    * Get Drawdown for drawdown id
    * 
    * @category Drawdown
    * @section API
    * @name get
    * 
    * @api public
    * 
    */
    
    function* get({
           $api,
           value:{drawDownId},
           $debug,
           $configuration
    }){
    try{
          const { drawdown } = yield $configuration('endpoints');
          const response = yield $api.get(`${drawdown}/${drawDownId}`);
          return response.body;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
    };
    
    module.exports=[schema,get];