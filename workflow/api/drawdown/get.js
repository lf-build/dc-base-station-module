var schema={
    applicationNumber:'string:min=2,max=100,required',
    };
    
    /**
    * 
    * Get Drawdown list for applicationNumber
    * 
    * @category Drawdown
    * @section API
    * @name get 
    * 
    * @api public
    * 
    */
    
    function* get({
           $api,
           value:{applicationNumber},
           $debug,
           $configuration
    }){
    try{
          const {'drawdown-filter': drawdown } = yield $configuration('endpoints');
          const response = yield $api.get(`${drawdown}/${applicationNumber}`);
          return response.body;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
    };
    
    module.exports=[schema,get];