var schema={
    applicationNumber:'string:min=2,max=100,required',
    };
    
    /**
    * 
    * Get Drawdown List
    * 
    * @category Drawdown
    * @section API
    * @name get draw-down list and application data
    * 
    * @api public
    * 
    */
    
    function* get({
           $api,
           value:{applicationNumber},
           $debug,
           $configuration,
           $stages:{
               api:{
                   application:{
                       filter :applicationFilter
                   },
                drawdown:{
                    get :getDrawdown
                }
               }
           }
    }){
    try{
        const response = yield getDrawdown.$execute
          .bind(undefined, {
              applicationNumber: applicationNumber,
          })({});

          const fresponse = yield applicationFilter.$execute
            .bind(undefined, {
                applicationNumber: applicationNumber,
            })({});
          const final= { application:fresponse.body[0], draws:response};

          return final;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
    };
    
    module.exports=[schema,get];