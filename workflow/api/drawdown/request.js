var schema={
      applicationNumber:'string:min=2,max=100,required',
      requestedAmount: 'number:min=0,precision=2',
    };
    
    /**
    * 
    * Request Drawdown 
    * 
    * @category Drawdown
    * @section API
    * @name request
    * 
    * @api public
    * 
    */
    
    function* get({
           $api,
           value:{applicationNumber,drawDownDate,requestedAmount,needByDate},
           $debug,
           $configuration
    }){
    try{
      var currentDate = new Date();      
          const payload={
            RequestedAmount: requestedAmount,
            LocNumber: applicationNumber,
            DrawDownDate: currentDate,
            NeedByDate: currentDate
          };

          const { 'drawdown-processor': drawdown } = yield $configuration('endpoints');
          const response = yield $api.post(`${drawdown}`,payload);
          return response.body;
        } catch(e) {
         $debug(e);
         throw {
           message: 'Something went wrong',
           code: 400,
      }
    }
    };
    
    module.exports=[schema,get];