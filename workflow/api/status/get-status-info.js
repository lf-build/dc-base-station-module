const schema = {
    entityType: 'string:required',
    workflowName: 'string:required',
    statusCode: 'string:required'
};
    /**
    * 
    * Return status details based on Workflow Name
    * 
    * @category Status
    * @section API
    * @name status details
    * 
    * @api public
    * 
    */
function* get({
    $debug,  
    $configuration,
    value:{entityType,workflowName,statusCode}
}){
    try{
        const statusManagement  = yield $configuration('status-management');
        const entityTypeInfo = statusManagement.entityTypes[entityType];
        let finalStatusResponse = {};
        if (entityTypeInfo && entityTypeInfo.StatusWorkFlows) {
            const workflowInfo = entityTypeInfo.StatusWorkFlows;
            const filteredWorkflow = workflowInfo.filter((x) => x.Name === workflowName);

        if (filteredWorkflow && filteredWorkflow.length > 0 && filteredWorkflow[0].statuses) {
            const statusInfo = filteredWorkflow[0].statuses.filter((p) => p.code === statusCode);
            if(statusInfo.length > 0) {
                finalStatusResponse = statusInfo[0];
            }
        }
        }
        return finalStatusResponse;
    } catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,get];