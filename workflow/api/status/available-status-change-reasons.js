const schema = {
    applicationNumber: 'string:required',
};
    /**
    * 
    * Return available status change reasons
    * 
    * @category Status
    * @section API
    * @name available reasons
    * 
    * @api public
    * 
    */
function* get({
    $debug,  
    $configuration,
    value:{applicationNumber},
    $stages:{
        'api':{
            'application':{
                'get-workflow':getWorkflow
            }
        }
    }
}){
    try{
        const statusMapping = yield $configuration('close-lost-status-mapping');
        const statusManagement  = yield $configuration('status-management');
        const stateArray =  Object.keys(statusMapping);
        const workflow = yield getWorkflow.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        const filteredStatusManagementObject = statusManagement.entityTypes.application.StatusWorkFlows.filter((p) => p.Name === workflow);
        const allStatus = filteredStatusManagementObject[0].statuses;
        let finalResponse = [];
        for(index in stateArray) {
            const statusName = stateArray[index];
            const filteredStatus = allStatus.filter((p) => p.code === statusMapping[statusName]);
            finalResponse.push({
                status: statusName,
                reasons: filteredStatus[0].reasons
            });
        }
        return finalResponse;
    } catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,get];