const schema = {
    applicationNumber: 'string:required',
};
    /**
    * 
    * Return all status details
    * 
    * @category Status
    * @section API
    * @name Status details
    * 
    * @api public
    * 
    */
function* get({
    $debug,  
    $configuration,
    value:{applicationNumber},
    $stages:{
        'api':{
            'application':{
                'get-workflow':getWorkflow
            }
        }
    }
}){
    try{
        const statusManagement  = yield $configuration('status-management');
        const status  = yield $configuration('borrower');
        const workflow = yield getWorkflow.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        const filteredStatusManagementObject = statusManagement.entityTypes.application.StatusWorkFlows.filter((p) => p.Name === workflow);
        const allStatus = filteredStatusManagementObject[0].statuses;
        let finalResponse = [];
        for(index in allStatus) {
            const statusCode = allStatus[index].code;
            const filteredStatus = allStatus[index].name;
            finalResponse.push({
                statusCode: statusCode,
                statusName: filteredStatus
            });
        }
        const response = {
            status: finalResponse,
            declinedStatus : status.declinedStatus
        }
        return response;
    } catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,get];