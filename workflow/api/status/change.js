const schema = {
    applicationNumber: 'string:required',
    newStatus: 'string:required',
    reasons:{
       '@items': 'string',
    },
    note:'string:required',
};
    /**
    * 
    * Change Status
    * 
    * @category Status
    * @section API
    * @name change
    * 
    * @api public
    * 
    */
function* get({
    $api,
    value:{applicationNumber, newStatus, reasons, note },
    $debug,  
    $configuration,
    $stages:{
        'api':{
            'application':{
                'get-workflow':getWorkflow
            }
        }
    }
}){
    try{
        const workflow = yield getWorkflow.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        const { 'status-management' : statusManagement } = yield $configuration('endpoints');
        const statusMapping = yield $configuration('close-lost-status-mapping');
        const payload = {
            note,
            reasons : reasons ? reasons : [],
        };
        const response = yield $api.put(`${statusManagement}/application/${applicationNumber}/${workflow}/${statusMapping[newStatus.toLowerCase()]}`,payload);
        return response.body;
    } catch(e) {
        $debug(e);
        throw e;
    }
}

module.exports=[schema,get];