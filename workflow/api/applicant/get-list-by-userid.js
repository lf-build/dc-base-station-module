var schema={
    userId:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{userId},
    $debug,
    $configuration
}){
   try{
    const { 'applicant-detail' : applicationDetail } = yield $configuration('endpoints');
    //return list of applicants by userId
    const response = yield $api.get(`${applicationDetail}/applicant/by/${userId}`);
    return response.body;
  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];