var schema={
    applicantId:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{applicantId},
    $debug,
    $configuration
}){
   try{
    const { 'applicant-detail' : applicationDetail } = yield $configuration('endpoints');
    //return applicant details by applicantId
    const response = yield $api.get(`${applicationDetail}/${applicantId}`);
    return response.body;
  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];