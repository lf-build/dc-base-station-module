const schema={
    applicationNumber:'string:min=2,max=100,required'
};

/**
* 
* Get required documents
* 
* @category Application Document
* @section API
* @name get required document
* 
* @api public
* 
*/

function* get({
  value:{applicationNumber},
  $api,
  $debug,
  $configuration,
  $stages: {
      'api': {
          'application': {
              'get-workflow': getWorkflow,
          }
      }
  },
}){
  const { 'verification-engine': verificationEngine } = yield $configuration('endpoints');
  try{
      let stipulationList = [];
      const response = yield getWorkflow.$execute
      .bind(undefined, {
          applicationNumber: applicationNumber,
      })({});
      const flowName =  (response && response !== '' ? `${response}/` : '');
      const factArray = yield $api.get(`${verificationEngine}/application/${applicationNumber}/${flowName}documents`);
      stipulationList = factArray.body;
      return stipulationList;
  }catch(e){
      $debug(e);
      throw {
          message: 'Something went wrong',
          code: 400,
      }
  }  
};

module.exports=[schema,get];