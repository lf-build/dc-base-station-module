var schema=({ $configuration }) => {  
    return $configuration('schema').then(schema => schema['other-owner-concent']);
};

/**
 * 
 * Update owner details
 * 
 * @category Application
 * @section API
 * @name update owner details
 * 
 * @api public
 * 
 */

function* get({
    $api,
    value,
    $debug,
    $configuration
}){
   try{
       const applicationNumber = value.applicationNumber;
       const { 'application-processor': application } = yield $configuration('endpoints');
       const payLoad = {owners: value.owners, ipAddress: value.ipAddress};
       const appResponse = yield $api.post(`${application}/application/${applicationNumber}/update/ownerconsent`,payLoad);
       return appResponse.body ;
  } catch(e) {
      $debug(e);
      throw e.status || { text: 'Something went wrong', code: 500 }
  }
};

module.exports=[schema,get];
