const schema={
    applicationNumber:'string:min=2,max=100,required',
    renewAmount:'number:positive,min=1,max=99999999,required',
    source:'string:required'
};

function* get({
    $api,
    value:{applicationNumber, renewAmount, source},
    $debug,
    $configuration
}){
    try{
        const { 'application-processor': applicationProcessor } = yield $configuration('endpoints');
        const payLoad = {
            "RenewalAmount": renewAmount,
            "RenewalSource": source
        };
        const response = yield $api.post(`${applicationProcessor}/application/${applicationNumber}/add/renewal`, payLoad);
        return response.body;
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];