const schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get application details
 * 
 * @category Application
 * @section API
 * @name get application details
 * 
 * @api public
 * 
 */

function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
        //micro service, return data in decrypted form
        const { 'application-filter': applicationFilters } = yield $configuration('endpoints');
        var response = yield $api.get(`${applicationFilters}/aggregate/${applicationNumber}`);
        return response.body;
    }
    catch(e){
        $debug(e);
        if(e.status.code === 404){
            throw{
                message: 'Application not found',
                code: 404,
            }
        }
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];