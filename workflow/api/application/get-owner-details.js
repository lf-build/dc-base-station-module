var schema={
    token:'string:required',
    userId: 'string:required',
};

/**
 * 
 * Get owner details
 * 
 * @category Application
 * @section API
 * @name get owner details
 * 
 * @api public
 * 
 */

function* get({
    $api,
    value:{token, userId},
    $debug,
    $configuration
}){
   try{
       const finalResponse = {};
       const { 'application-processor': applicationProcessor } = yield $configuration('endpoints');
       const tokenResponseBody = yield $api.post(`${applicationProcessor}/application/checktoken`, { "Token": token });
       const applicationNumber = tokenResponseBody.body;
       const consentSignedResponse = yield $api.post(`${applicationProcessor}/application/${applicationNumber}/checkconsentsign/${userId}`);
       finalResponse.ownerSignDone = consentSignedResponse.body;
       const appResponse = yield $api.get(`${applicationProcessor}/application/${applicationNumber}/details`);
       const ownerDetail = appResponse.body.owners.filter((x) => x.ownerId === userId);
       finalResponse.ownerDetail = ownerDetail;
       finalResponse.applicationNumber = applicationNumber;
       return finalResponse ;
  } catch(e) {
      $debug(e);
      throw e.status || { text: 'Something went wrong', code: 400 }
  }
};

module.exports=[schema,get];
