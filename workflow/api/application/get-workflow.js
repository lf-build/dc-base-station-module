const schema={
    applicationNumber:'string:min=2,max=100,required'
};

function* get({
  value:{applicationNumber},
  $api,
  $debug,
  $configuration,
  $stages: {
      'api': {
          'application': {
              'filter': applicationFilter,
          }
      }
  },
}){
  try{
      let flowName = '';
      const response = yield applicationFilter.$execute
      .bind(undefined, {
          applicationNumber: applicationNumber,
      })({});
      if(response.body && response.body.length > 0){
          if(response.body[0].StatusWorkFlowId && response.body[0].StatusWorkFlowId != '' && response.body[0].StatusWorkFlowId != undefined && response.body[0].StatusWorkFlowId != null){
              flowName = response.body[0].StatusWorkFlowId;
          }
      }
      return flowName;
  }catch(e){
      $debug(e);
      throw {
          message: 'Something went wrong',
          code: 400,
      }
  }  
};

module.exports=[schema,get];