var schema={
    gstin:'string:min=2,max=100,required',
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{gstin, applicationNumber},
    $debug,
    $configuration
}){
    try{
          const { 'application-processor': application } = yield $configuration('endpoints');
          const response = yield $api.get(`${application}/application/${applicationNumber}/gst/details/${gstin}`);
          return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];