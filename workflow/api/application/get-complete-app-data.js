var schema = {
    userId: 'string:min=2,max=100,required',
    applicantId: 'string',
    applicationNumber: 'string',
};

function* get({
    value: { userId, applicantId, applicationNumber },
    $debug,
    $stages: {
        'api': {
            'ca': {
                'get-application-by-applicantid': getApplication,
            },
            'application': {
                'filter': applicationFilter,
                'get-business-application': getBusinessApplication
            },
            'applicant': {
                'get-list-by-userid': getApplicantList,
                'get': getApplicant
            },
        }
    }
}) {
    try {
        let finalResponse = [];
        
        let applicantList = [];
        if(applicantId){
            //get applicant data from applicantId
            const applicant = yield getApplicant.$execute.bind(undefined, { applicantId })({});
            applicantList.push(applicant);
        }else{
            //get list of applicants from userid
            applicantList = yield getApplicantList.$execute.bind(undefined, { userId })({});
        }  

        //loop through applicants to fetch their applications
        for(index in applicantList) {
            const applicant = applicantList[index];

            let businessApplications = [];
            if(applicationNumber){
                //get businessApplication by applicationNumber
                application = yield getBusinessApplication.$execute.bind(undefined, { applicationNumber })({});
                businessApplications.push(application);
            }else{
                //get businessApplication list by applicantId
                businessApplications = yield getApplication.$execute.bind(undefined, { applicantId: applicant.id })({});
            }

            for(i in businessApplications) {
                const businessApplication = businessApplications[i];

                //get application-filter data
                const filterResponse = yield applicationFilter.$execute
                .bind(undefined, {
                    applicationNumber: businessApplication.applicationNumber,
                })({});

                finalResponse.push({applicant, businessApplication, filterApplication: filterResponse.body[0]});
            }
        }
        return finalResponse;
    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];