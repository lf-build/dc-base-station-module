const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{

        //node service,return ssn in encrypted form, tangs included in response

        const { 'application-filters': applicationFilters } = yield $configuration('endpoints');
        const filter = {
            "searchFilterView": {
                "ApplicationNumber": applicationNumber
            }
        };
        return yield $api.post(`${applicationFilters}/application/search-applications-free`, filter);
    }
    catch(e){
        $debug(e);
        if(e.status.code === 404){
            throw{
                message: 'Application not found',
                code: 404,
            }
        }
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];