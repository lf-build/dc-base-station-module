var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Check application eligibility for reapply 
 * 
 * @category Application
 * @section API
 * @name check reapply eligibility
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
     try{
        const { 'application-processor': application } = yield $configuration('endpoints');
        const finalURL = application+"/application/"+applicationNumber+"/re_apply_check";
        const response = yield $api.get(finalURL);
        return response.body;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];