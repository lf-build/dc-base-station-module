var schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
   try{
    const { application } = yield $configuration('endpoints');
    // returns business application by applicationNumber
    const response = yield $api.get(`${application}/${applicationNumber}`);
    return response.body;
  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];