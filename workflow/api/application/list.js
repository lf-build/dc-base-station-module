const schema = {
    referenceId: 'number',
    pageSize: 'number',
    sourceType: 'string',
};

/**
 * 
 * Get application list
 * 
 * @category Application
 * @section API
 * @name get application list
 * 
 * @api public
 * 
 */

function * get({
    $api,
    value: {
        referenceId, pageSize,sourceType,
    },
    $debug,
    $configuration
}) {
    try{
        const { 'application-filters': applicationFilters } = yield $configuration('endpoints');
        const filter = {
            "searchFilterView": {},
            "sorts": {
                'ApplicationDate.Time.Tick' : 1
            }
        };

        let filterObj = {};
        if (referenceId && referenceId !== null && referenceId !== "") {
            Object.assign(filterObj,{
                        'ApplicationDate.Time.Ticks' : {
                            "$gt": referenceId,
                        }
                    });
        }
        if (sourceType && sourceType !== null && sourceType !== "") {
            Object.assign(filterObj,{
                            "SourceType": sourceType,
                    });
        }
        filter.searchFilterView = filterObj;
    
        const response = yield $api.post(`${applicationFilters}/application/search-applications-free?page=${1}&pageSize=${pageSize || 100}`, filter);
        return {
            referenceId: response.body.length > 0 ? response.body[response.body.length - 1].ApplicationDate.Time.Ticks : referenceId,
            items: response.body,
            pageSize: response
                .headers
                .get('X-PAGE-SIZE'),
            totalCount: response
                .headers
                .get('X-TOTAL-COUNT'),
            totalPages: response
                .headers
                .get('X-TOTAL-PAGES')

        };
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];