var schema = {
    userId: 'string:min=2,max=100,required',
    applicantId: 'string',
    applicationNumber: 'string',
    onlyDashboardGrid: 'boolean',
    isloanDataRequired: 'boolean',
};

function* get({
    value: { userId, applicantId, applicationNumber, onlyDashboardGrid, isloanDataRequired },
    $debug,
    $configuration,
    $stages: {
        'api': {
            'ca': {
                'get-application': getAppicationData,
            },
            'application': {
                'get-complete-app-data': getAllAppData,
                'check-reapply-eligibility': reapplyCheck
            },
            'lookup':{
                'get': getLookupByName
            },
            'drawdown':{
                'get': getDrawdownList
            },
            'status':{
                'get-status-info': getStatusInfo
            },
            'loan-management':{
                'get-loan-by-application': getLoanByAppNo,
                'get-schedule-info': getScheduleInfo,
            },
            'loan-accounting':{
                'get-economics-data': getEconomics
            },
        }
    }
}) {
    try {
        let finalResponse = {};
        let user = {};
        const borrowerConfigResponse = yield $configuration('borrower');

        // get all application, loan, applicant, filterApplication from userid
        const payload = { userId, applicantId, applicationNumber };
        const rowData = yield getAllAppData.$execute.bind(undefined, payload)({});
        let masterData = [];
        for(i in rowData) {
            let completeApp = rowData[i];
            //add isDeclined & isLead in filterApplication
            completeApp.filterApplication.isDeclined = borrowerConfigResponse.declinedStatus.filter((x) => x === completeApp.filterApplication.StatusCode).length === 1;
            completeApp.filterApplication.isLead = borrowerConfigResponse.leadStatus === completeApp.filterApplication.StatusCode;

            completeApp.loanInfo = null;
            if(isloanDataRequired && isloanDataRequired === true && !completeApp.filterApplication.isLead && !completeApp.filterApplication.isDeclined){
                try{
                    completeApp.loanInfo = yield getLoanByAppNo.$execute.bind(undefined, {
                        applicationNumber:completeApp.filterApplication.ApplicationNumber,
                        })({});
                }catch(ex){
                    $debug(ex);
                }
            }
            completeApp.economicsData = null;
            completeApp.scheduleInfo = null;
            if(completeApp.loanInfo !== undefined && completeApp.loanInfo !== null)
            {
                let loanNumber=completeApp.loanInfo.loanNumber;
                const scheduleInfo = yield getScheduleInfo.$execute
                    .bind(undefined, {
                        loanNumber,
                    })({});
                completeApp.scheduleInfo=scheduleInfo;

                const economicsData =  yield getEconomics.$execute
                .bind(undefined, {
                    loanNumber,
                    scheduleVersion : scheduleInfo.scheduleVersionNo
                })({});
                
                completeApp.economicsData=economicsData;
            }
            masterData.push(completeApp);
        }
        // start - create dashboardData array
        let dashboardData = [];
        if(applicationNumber === undefined || applicationNumber === null){
            for(i in masterData) {
                const completeApp = masterData[i];
                const dashboardEntity = {
                    userId,
                    applicantId: completeApp.applicant.id,
                    legalBusinessName: completeApp.applicant.legalBusinessName ? completeApp.applicant.legalBusinessName : '',
                    contactFirstName: completeApp.businessApplication.contactFirstName ? completeApp.businessApplication.contactFirstName : '',
                    contactLastName: completeApp.businessApplication.contactLastName ? completeApp.businessApplication.contactLastName : '',
                    applicationDate: completeApp.businessApplication.applicationDate.time,
                    applicationNumber: completeApp.businessApplication.applicationNumber,
                    statusCode: completeApp.filterApplication.StatusCode,
                    isLead: completeApp.filterApplication.isLead,
                    isDeclined: completeApp.filterApplication.isDeclined,
                    loanNumber: completeApp.loanInfo && completeApp.loanInfo.loanNumber ? completeApp.loanInfo.loanNumber : null,
                    loanFundedDate: completeApp.loanInfo && completeApp.loanInfo.loanFundedDate ? completeApp.loanInfo.loanFundedDate.time : null,
                    economicsData: completeApp.economicsData,
                    scheduleInfo: completeApp.scheduleInfo,
                    drawDownDetails: completeApp.loanInfo.drawDownDetails,
                    businessMaxLimitAmount: completeApp.loanInfo.primaryBusinessDetails.businessMaxLimitAmount,
                    borrowerId:completeApp.loanInfo.borrowerId,
                };
                dashboardData.push(dashboardEntity);
            }
            finalResponse.dashboardData = dashboardData;
            if(onlyDashboardGrid && onlyDashboardGrid === true){
                return finalResponse;
            }
        }
        // end - create dashboardData array

        // start - Find active application
        let activeAppicationNumber = '';
        if(applicationNumber === undefined || applicationNumber === null){
            if(dashboardData && dashboardData.length === 1){
                activeAppicationNumber = dashboardData[0].applicationNumber;
            } else if((dashboardData.filter((x) => x.isDeclined === true).length === 0) || (dashboardData.filter((x) => x.isDeclined === false).length === 0)){
                //all active/ all declined applications, pick by last applicationDate
                const obj = dashboardData.reduce(function (a, b) { return (new Date(a.applicationDate)).getTime() > (new Date((b .applicationDate))).getTime() ? a : b});
                activeAppicationNumber = obj.applicationNumber;
            } else{
                //list contains both type of applications, filer active applications and pick by last applicationDate
                const allActiveApps = dashboardData.filter((x) => x.isDeclined === false);
                // single active application
                if(allActiveApps && allActiveApps.length === 1){
                    activeAppicationNumber = allActiveApps[0].applicationNumber;
                } else{ // list of active applications
                    const obj = allActiveApps.reduce(function (a, b) { return (new Date(a.applicationDate)).getTime() > (new Date((b .applicationDate))).getTime() ? a : b});
                    activeAppicationNumber = obj.applicationNumber;
                }
            }
        } else{
            activeAppicationNumber = applicationNumber;
        }
        const appProcessorData = yield getAppicationData.$execute
                .bind(undefined, {
                    applicationNumber: activeAppicationNumber,
            })({});
        finalResponse.application = appProcessorData;
        // end - Find active application

        // start - User data
        
        //get purpose of loan from lookup
        const sourceTypeLookup = yield getLookupByName.$execute.bind(undefined, { name: 'SourceType' })({});
        const purposeOfLoanLookup = yield getLookupByName.$execute.bind(undefined, { name: 'PurposeOfLoan' })({});

        const activeMasterObj = masterData.filter((x) => x.businessApplication.applicationNumber === activeAppicationNumber);
        user = {
            userId,
            applicationNumber: activeAppicationNumber,
            lmsAccess: borrowerConfigResponse && borrowerConfigResponse.lmsAccess && borrowerConfigResponse.lmsAccess === true ? true : false,
            renewAccess: borrowerConfigResponse && borrowerConfigResponse.enableRenewFeature && borrowerConfigResponse.enableRenewFeature === true ? true : false,
            maxOwnersCount: borrowerConfigResponse.applicationCreation.owners.maxOwnersCount,
            offerTabForMCA: borrowerConfigResponse && borrowerConfigResponse.enableOfferTabForMCA && borrowerConfigResponse.enableOfferTabForMCA === true ? true : false,
            offerGenerateStatusCode: borrowerConfigResponse && borrowerConfigResponse.mcaOfferGenerateStatusCode ? borrowerConfigResponse.mcaOfferGenerateStatusCode : '',
            pages: borrowerConfigResponse && borrowerConfigResponse.pages ? borrowerConfigResponse.pages : null,
            applicantId: activeMasterObj[0].applicant.id,
            applicationDate: activeMasterObj[0].businessApplication.applicationDate.time,
            code: activeMasterObj[0].filterApplication.StatusCode,
            email: activeMasterObj[0].businessApplication.primaryEmail.email,
            loanAmount: activeMasterObj[0].businessApplication.requestedAmount ? activeMasterObj[0].businessApplication.requestedAmount:'',
            source: activeMasterObj[0].businessApplication.source && activeMasterObj[0].businessApplication.source.sourceType ? sourceTypeLookup[activeMasterObj[0].businessApplication.source.sourceType] : '',
            loanPurpose: activeMasterObj[0].businessApplication.purposeOfLoan ? purposeOfLoanLookup[activeMasterObj[0].businessApplication.purposeOfLoan]:'',
            phoneNumber: activeMasterObj[0].businessApplication.primaryPhone && activeMasterObj[0].businessApplication.primaryPhone.phone ? activeMasterObj[0].businessApplication.primaryPhone.phone:'',
            productId: activeMasterObj[0].filterApplication.ProductId,
            isDeclined: activeMasterObj[0].filterApplication.isDeclined, 
            isLead: activeMasterObj[0].filterApplication.isLead, 
            productPortfolioType: activeMasterObj[0].filterApplication.PortfolioType,
            status: activeMasterObj[0].filterApplication.StatusName,
            tags: activeMasterObj[0].filterApplication.tags && activeMasterObj[0].filterApplication.tags.length > 0 ? activeMasterObj[0].filterApplication.tags : [],
            loanNumber: activeMasterObj[0].loanInfo ? activeMasterObj[0].loanInfo.loanNumber : null,
        };

        let userName = activeMasterObj[0].businessApplication.contactFirstName ? activeMasterObj[0].businessApplication.contactFirstName : '';
        userName = activeMasterObj[0].businessApplication.contactLastName ? `${userName} ${activeMasterObj[0].businessApplication.contactLastName}` : userName;
        user.userName = userName;

        let isDrawFunded = false;
        const checkDrawdown = borrowerConfigResponse.drawdownPortfolios && user.productPortfolioType && borrowerConfigResponse.drawdownPortfolios.lenght > 0 && borrowerConfigResponse.drawdownPortfolios.filter((p) => p.toLowerCase() === user.productPortfolioType.toLowerCase()).length === 1 ? true : false;
        if(checkDrawdown){
            try{
                const drawDownList = yield getDrawdownList.$execute.bind(undefined, {
                        applicationNumber:activeAppicationNumber,
                        })({});
                const drawdownStatusInfo = yield getStatusInfo.$execute.bind(undefined, {
                    entityType: borrowerConfigResponse.drawdownStats.entityType,
                    workflowName: borrowerConfigResponse.drawdownStats.workflowName,
                    statusCode: borrowerConfigResponse.drawdownStats.fundingStatusCode
                    })({});
                    
                if(drawdownStatusInfo && drawdownStatusInfo.name && drawDownList && drawDownList.length > 0 && drawDownList.filter((p) => p.statusName === drawdownStatusInfo.name).length > 0){
                    isDrawFunded = true;
                }
            }catch(e){
                $debug(e);
            }
        }
        user.isDrawFunded = isDrawFunded;
        user.drawdownPortfolios = borrowerConfigResponse.drawdownPortfolios;

        let isEligibleForReapply = false;
        const reapplyFeature = borrowerConfigResponse && borrowerConfigResponse.enableReapplyFeature && borrowerConfigResponse.enableReapplyFeature === true ? true : false;
        if(!user.isLead && reapplyFeature){
            // Check for reapply Eligibility
            try{
                const checkReapplyEligibility = yield reapplyCheck.$execute
                        .bind(undefined, {
                        applicationNumber:activeAppicationNumber,
                        })({});
                isEligibleForReapply = checkReapplyEligibility;
            }catch(e){
                $debug(e);
            }
        }
        user.isEligibleForReapply = isEligibleForReapply;

        finalResponse.user = user;
        // end - user data

        return finalResponse;
    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];