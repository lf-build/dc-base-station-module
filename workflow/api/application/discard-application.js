var schema={
    applicationNumber:'string:min=2,max=100,required',
    rejectCode:'string:min=1,max=10,required',
};

function* get({
    $api,
    value:{applicationNumber, rejectCode},
    $debug,
    $configuration,
    $stages: {
      'api': {
        'application': {
          'get-workflow': getWorkflow,
        }
      }
    },
}){

  try{
    const response = yield getWorkflow.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});
    const flowName =  (response && response !== '' ? `${response}/` : '');
    const { 'status-management': statusManagement } = yield $configuration('endpoints');
    const Response = yield $api.put(`${statusManagement}/application/${applicationNumber}/${flowName}${rejectCode}`);
    return Response;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Transition not allowed',
      code: 400,
    }
  }
};

module.exports=[schema,get];