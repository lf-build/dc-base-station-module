var schema = {
    loanNumber: 'string:min=2,max=100,required',
};

function* get({
    $api,
    value: { loanNumber },
    $debug,
    $configuration,
    $stages: {
        'api': {
            'loan-accounting':{
                'get-economics-data': getEconomics
            },
            'loan-management':{
                'get-schedule-info': getScheduleInfo
            }
        }
    }
}) {
    try {
        let finalResposne = {};

        const lmsConfig = yield $configuration('LMS');
        finalResposne.estimatePayOffDays = lmsConfig.EstimatePayoffDays;

        const versionResponse = yield getScheduleInfo.$execute
            .bind(undefined, {
                loanNumber,
            })({});
        
        const economicsData =  yield getEconomics.$execute
        .bind(undefined, {
            loanNumber,
            scheduleVersion : versionResponse.scheduleVersionNo
        })({});
        finalResposne.economicsData = economicsData;

        return finalResposne;

    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];