var schema = {
};

function* get({
    $api,
    value,
    $debug,
    $configuration,
    $stages: {
        'api': {
            'ca': {
                'lookup': lookup
            }
        }
    }
}) {
    try {
        let finalResponse = {};
        
        //get purpose of loan from lookup
        const lookups = yield lookup.$execute
        .bind(undefined, {})({});

        finalResponse.lookups = lookups;
       
        if(lookups && lookups.ApplicationPath){
            const applicationPathData = yield $configuration('applicationPathData');
            finalResponse.applicationPathData = applicationPathData;
        }

        return finalResponse;
    } catch (e) {
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];