var schema={
    applicantId:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{applicantId},
    $debug,
    $configuration
}){
   try{
    const { application } = yield $configuration('endpoints');
    // returns list of application by applicantid
    const response = yield $api.get(`${application}/application/by/${applicantId}`);
    return response.body;
  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];