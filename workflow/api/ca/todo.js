const schema={
      applicationNumber:'string:min=2,max=100,required'
};


/**
 * 
 * Get Todo page document list
 * 
 * @category TODO page
 * @section API
 * @name get
 * 
 * @api public
 * 
 */


function* get({
    value:{applicationNumber},
    $api,
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter,
        'required-document': stipulationDoc,
      },
      'document':{
        'get-fact-documents': getFactDocuments,
      }
    }
  },
}){
    const {'verification-engine': verificationEngine } = yield $configuration('endpoints');

    //Fetch list of addition stipulation to show
    let taxReturnVerification = false;
    let confessionVerification = false;
    let lienVerification = false;
    let voidedCheck = false;
    let photoId = false;
    let contract = false;
    let proofOfProperty = false;
    let merchantStatementVerification = false;
    try{
        const stipulationList = yield stipulationDoc.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(stipulationList.length > 0){
            stipulationList.forEach(function(element) {
                if(element.factName === 'TaxReturnVerification'){
                   taxReturnVerification = element.isRequested;
                }
                else if(element.factName === 'ConfessionVerification'){
                   confessionVerification = element.isRequested;
                }
                else if(element.factName === 'LienVerification'){
                   lienVerification = element.isRequested;
                }
                else if(element.factName === 'BankVerification'){
                   voidedCheck = element.isRequested;
                }
                else if(element.factName === 'IDVerification'){
                   photoId = element.isRequested;
                }
                else if(element.factName === 'ProofOfPropertyOwnerShipVerification'){
                    proofOfProperty = element.isRequested;
                 }
                else if(element.factName === 'SignedDocumentVerification'){
                   contract = element.isRequested;
                }
                else if(element.factName === 'MerchantStatementVerification'){
                    merchantStatementVerification = element.isRequested;
                 }
            }, this);
        } 
    }catch(e){
        $debug(e);
    }

    //fetch Voided check documents
    let voidedCheckDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'BankVerification' })({});
        voidedCheckDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            voidedCheck = true;
        }
    }catch(e){
        $debug(e);
    }

    //fetch PhotoId documents
    let photoIdDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'IDVerification' })({});
        photoIdDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            photoId = true;
        }
    }catch(e){
        $debug(e);
    }

    //fetch Contract documents
    let contractDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'SignedDocumentVerification' })({});
        contractDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            contract = true;
        }
    }catch(e){
        $debug(e);
    }

    //fetch Tax return documents
    let taxReturnVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'TaxReturnVerification' })({});
        taxReturnVerificationDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            taxReturnVerification = true;
        }
    }catch(e){
       $debug(e);
    }

    //fetch lien documents
    let lienVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'LienVerification' })({});
        lienVerificationDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            lienVerification = true;
        }
    }catch(e){
        $debug(e);
    }

     //fetch confession documents
    let confessionVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'ConfessionVerification' })({});
        confessionVerificationDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            confessionVerification = true;
        }
    }catch(e){
        $debug(e);
    }

    let email = '';
    let docuSignStatus = 'Signed';
    try{
       const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(response.body && response.body.length > 0){
            email = (response.body[0].Owners && response.body[0].Owners.length > 0) ? response.body[0].Owners[0].EmailAddress : '';
            if(response.body[0].tags && response.body[0].tags.length > 0){
                if(response.body[0].tags.filter((p) => p === 'Pending Signature').length > 0){
                    docuSignStatus = 'Unsigned';
                }
            }
        }
    }
    catch(e){
         $debug(e);
    }

    let proofOfPropertyDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'ProofOfPropertyOwnerShipVerification' })({});
        proofOfPropertyDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            proofOfProperty = true;
        }
    }catch(e){
        $debug(e);
    }

    let merchantStatementVerificationDocs = []; 
    try{
        const factRespsone = yield getFactDocuments.$execute.bind(undefined, { applicationNumber: applicationNumber, factName: 'MerchantStatementVerification' })({});
        merchantStatementVerificationDocs = factRespsone.files;
        if(factRespsone.files.length > 0){
            merchantStatementVerification = true;
        }
    }catch(e){
        $debug(e);
    }
    return {
        "voidedCheck":{
            "status":voidedCheckDocs.length > 0 ? true : false,
            "files":voidedCheckDocs,
            "isDisplay":voidedCheck,
        },
        "photoId":{
            "status":photoIdDocs.length > 0 ? true : false,
            "files":photoIdDocs,
            "isDisplay":photoId,
        },
        "contract":{
            "status": (contractDocs.length > 0 || docuSignStatus === 'Signed' ) ? true : false,
            "files":contractDocs,
            docuSignStatus,
            "email": email,
            "isDisplay":contract,
        },
        "taxReturn":{
            "status":taxReturnVerificationDocs.length > 0 ? true : false,
            "files":taxReturnVerificationDocs,
            "isDisplay":taxReturnVerification,
        },
        "lien":{
            "status":lienVerificationDocs.length > 0 ? true : false,
            "files":lienVerificationDocs,
            "isDisplay":lienVerification,
        },
        "confession":{
            "status":confessionVerificationDocs.length > 0 ? true : false,
            "files":confessionVerificationDocs,
            "isDisplay":confessionVerification,
        },
        "proofOfProperty":{
            "status":proofOfPropertyDocs.length > 0 ? true : false,
            "files":proofOfPropertyDocs,
            "isDisplay":proofOfProperty,
        },
        "merchant":{
            "status":merchantStatementVerificationDocs.length > 0 ? true : false,
            "files":merchantStatementVerificationDocs,
            "isDisplay":merchantStatementVerification,
        },
    };
};

module.exports=[schema,get];