const schema = ({ $configuration }) => {  
  return $configuration('schema').then(schema => schema['lead']);
}

/**
 * 
 * Create lead
 * 
 * @category Lead
 * @section API
 * @name create lead
 * 
 * @api public
 * 
 */

function* set({
    $api,
    value, 
    $debug,  
    $configuration
}){
  try {
    if (value.IsReapply) {
      return value;
    }
    else {
      const { 'application-processor': application } = yield $configuration('endpoints');
      const appResponse = yield $api.post(`${application}/application/lead`, value);
      return appResponse.body;
    }

  } catch(e) {
    $debug(e);
    if(e && e.body && e.body.code && e.body.code === 409){
      throw {
        message: 'Email Address already exist.',
        code: e.body.code,
      }
    }
    throw {
      message: e && e.body && e.body.message ? e.body.message : 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,set];