var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get Plaid linked bank details and Bank Statement documents
 * 
 * @category Bank Linking
 * @section API
 * @name get  
 * 
 * @api public
 * 
 */

function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
        'api': {
          'application': {
            'filter': applicationFilter
          },
          'ibv': {
            'get-plaid-token': getPlaidPublicToken
          }
        }
      },
}){
   try{
    const { 'application-processor': application , 'document-service': documentService } = yield $configuration('endpoints');
    const borrowerConfigResponse = yield $configuration('borrower');

    let finalResponse = {};

    const plaidResponse = yield $configuration(borrowerConfigResponse.plaidConfigName);
    let plaid = {};
    plaid.apiVersion=plaidResponse.apiVersion;
    plaid.clientName=plaidResponse.clientName;
    plaid.env=plaidResponse.env;
    plaid.key=plaidResponse.key;
    plaid.product=plaidResponse.product;
    plaid.selectAccount=plaidResponse.selectAccount;
    plaid.webhookURL=plaidResponse.WebhookUrl;
    
    finalResponse.plaid = plaid;

    try{     
      const accounts = yield $api.get(`${application}/application/${applicationNumber}/account/cashflow`);
      finalResponse.accounts = accounts.body && accounts.body !== "" && accounts.body !== null ? accounts.body : {};
    } catch(e){
        $debug(e);
        finalResponse.accounts = {};
    }

    let voidedCheckDocs = []; 
    try{
        const voidedCheckFiles = yield $api.get(`${documentService}/${applicationNumber}/bankstatement`);
        for(index in voidedCheckFiles.body){
                const voidedCheckDoc = {
                    fileName: voidedCheckFiles.body[index].fileName,
                    fileId: voidedCheckFiles.body[index].id,
                }
                voidedCheckDocs.push(voidedCheckDoc);
        }
    }catch(e){
        $debug(e);
    }
    
     const response = yield applicationFilter.$execute
    .bind(undefined, {
        applicationNumber: applicationNumber,
    })({});

    finalResponse.voidedCheck = {
            "status":voidedCheckDocs.length > 0 ? true : false,
            "files":voidedCheckDocs,            
    };

    finalResponse.tags = (response.body && response.body.length>0 && response.body[0].tags) ? response.body[0].tags : [];

    finalResponse.plaidToken = {};

    if(finalResponse.tags.length > 0 && finalResponse.tags.includes('Plaid Bank Linking Refresh')){
        try{
            const response = yield getPlaidPublicToken.$execute
            .bind(undefined, {
                applicationNumber: applicationNumber,
            })({});
            finalResponse.plaidToken = response;
        }catch(ex){
            $debug(ex);
        }
    }
    
    return finalResponse;
  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];