var schema = {
    userId:'string:min=2,max=100,required',
    userName:'string:min=2,max=100,required',
    partnerId:'string:min=2,max=100,required',
  };
  
  function *get({
      $get,
      $set,
      $api,
      value:{userId,userName,partnerId},
      $debug,
      $configuration
  })  
  {
    const { partners: brokerEndpoint , identity: identityEndpoint, 'asset-service': assetEndpoint} = yield $configuration('endpoints');
    let finalResponse = {};
    finalResponse.email = userName;
  
    //Fetch partnerId from UserId - working
    //const PartnerIdResponse = yield $api.get(`${brokerEndpoint}/${userId}/partnerdetails`);
    //const partnerId = PartnerIdResponse.body.partnerId;
  
    //Fetch Max owner count
    const borrowerConfigResponse = yield $configuration('borrower');

    //Fetch name
    let brokerName = '';
    try{
      const response = yield $api.get(`${brokerEndpoint}/${partnerId}`);
      brokerName = `${response.body.principalOwner.firstName} ${response.body.principalOwner.lastName}`;
    }
    catch(e){
      $debug(e);
    }
    finalResponse.name = brokerName;
  
    //Fetch Image
    let brokerImage = null;
    try{
      const imgResponse = yield $api.get(`${assetEndpoint}/user/${userName}/profile.png`);
      brokerImage = imgResponse.body.url;
    }
    catch(e){
      $debug(e);
    }
    finalResponse.image = brokerImage;
    finalResponse.userId = userId;
    finalResponse.partnerId = partnerId;
    finalResponse.maxOwnersCount = borrowerConfigResponse.applicationCreation.owners.maxOwnersCount;
  
    return finalResponse;
  };
  
  module.exports = [schema, get];