var FormData = require('form-data');
const schema = {
   applicationNumber: 'string:min=2,max=100,required',
   fileList: {
        '@items': {
          file: 'string:min=2,max=100,required',
          dataUrl: 'string:min=2,max=100000000,required',
        },
        max: 20,
        min: 1,
    }
};

function *set({
  $api, 
  value:{
    applicationNumber,
    fileList,
  },
  $debug,
  $configuration,
  $stages: {
    'api': {
      'perfect-audit': {
        'create-book': createBook,
      }
    }
  },
}){

  const { 'perfect-audit': perfectAudit } = yield $configuration('endpoints');

  try{
    const createBookResponse = yield createBook.$execute.bind(undefined, {applicationNumber})({});
    for(index in fileList) {
            let fileObject = fileList[index];
            const formData = new FormData();
            formData.append('file', new Buffer(fileObject.dataUrl.substring(fileObject.dataUrl.indexOf("base64,")).replace('base64,',''), 'base64'), fileObject.file);
            const response = yield $api.post(`${perfectAudit}/application/${applicationNumber}/uploadstatement`, formData, null); 
    }
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  } 
};

module.exports = [schema, set];