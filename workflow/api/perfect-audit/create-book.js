const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
}){
    try{
        const { 'perfect-audit': perfectAudit } = yield $configuration('endpoints');
        const response = yield $api.post(`${perfectAudit}/application/${applicationNumber}/createbook`);
        return response.body;
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];